var csv = {};

$(document).ready(function() {
	
	google.charts.load('current', {'packages':['corechart', 'bar', 'line', 'controls']});

	var marker = null;

	var map = L.map('map', {
		center: [10.83, -72.66],
		zoom: 8,
		maxZoom: 18,
	});
	map.on('contextmenu', function(e) {
		return false; 
	});

	L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
		attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
	}).addTo(map);

	var bounds = [[10.97, -73.05], [10.83, -72.66]];
	map.fitBounds(bounds);

	console.log(L.version);


		map.on('mousemove click', function (e) {
			var coordenadas = e.latlng.toString();
			var res = coordenadas.replace("LatLng(", "");
			var res = res.replace(")", "");
			var res = res.split(", ");
			lat_string = degToDMS(res[0], 'LAT');
          	lon_string = degToDMS(res[1], 'LON');

			$(".latitud").html(lat_string);
			$(".longitud").html(lon_string);
	});

	$('#variable').select2();
	$( "#datepicker" ).datepicker();

/*	$('#fromDate').on("keypress, keydown, keyup", function(e) {
		alert('sisas');
		$(".lightpick").hide();
	});
	$('#toDate').on("keypress, keydown, keyup", function(e) {
		alert('sisas');
		$(".lightpick").hide();
	});*/


	// add a scale at at your map.
	var scale = L.control.scale().addTo(map); 

	// Get the label.
	var metres = scale._getRoundNum(map.containerPointToLatLng([0, map.getSize().y / 2 ]).distanceTo( map.containerPointToLatLng([scale.options.maxWidth,map.getSize().y / 2 ])))
	  label = metres < 1000 ? metres + ' m' : (metres / 1000) + ' km';

	map.on("zoomend", function (e) { 
	  console.log("ZOOMEND", e); 
	  var metres = scale._getRoundNum(map.containerPointToLatLng([0, map.getSize().y / 2 ]).distanceTo( map.containerPointToLatLng([scale.options.maxWidth,map.getSize().y / 2 ])))
	  label = metres < 1000 ? metres + ' m' : (metres / 1000) + ' km';
	  console.log(metres);
	  $(".escala").html("1:"+ metres*100);
	});
	//$(".leaflet-top").addClass("leaflet-right").removeClass("leaflet-left");

/*	L.easyButton( '<span class="star">&starf;</span>', function(){
		alert('you just clicked the html entity \&starf;');
	}).addTo(map);*/

	    // our dialogs; one is modal and the other is not and that's okay
		$('#dialog-about').dialog({
        modal: false, autoOpen: true, closeOnEsc: true, draggable: true,
        width: '30%',

        height: ($(window).height() - 200),
        position: { my:'left top', at:'left+50 top+50', or:'#map' }
    });    
    // the map controls to toggle those dialogs
    new L.Control.jQueryDialog({
        dialogId: 'dialog-about',
        tooltip: "Consultar Indices",
				iconClass: 'fa fa-list-ul',
				minimizable:true
    }).addTo(map);

		$( "#tabs" ).tabs();

	var fromDate = new Lightpick({ field: document.getElementById('fromDate'), maxDate: moment(), format: 'YYYY-MM-DD', lang: 'es' });  
	var toDate = new Lightpick({ field: document.getElementById('toDate'), maxDate: moment(), format: 'YYYY-MM-DD', lang: 'es' });

	
	$("#variable").change(function() {
		$("#fromDate").val('');
		$("#toDate").val('');
		var val = $(this).val();
		if (val === 'NDVI-LANDSAT') {
			fromDate.reloadOptions({ minDate: '2013-02-11', maxDate: moment(), format: 'YYYY-MM-DD', lang: 'es'});
			toDate.reloadOptions({ minDate: '2013-02-11', maxDate: moment(), format: 'YYYY-MM-DD', lang: 'es'});
		} else if (val === 'NDVI-SENTINEL') {
			fromDate.reloadOptions({ minDate: '2015-07-15', maxDate: moment(), format: 'YYYY-MM-DD', lang: 'es'});
			toDate.reloadOptions({ minDate: '2015-07-15', maxDate: moment(), format: 'YYYY-MM-DD', lang: 'es'});
		} else if (val === 'PRECIPITATION') {
			fromDate.reloadOptions({ minDate: '1981-01-01', maxDate: moment(), format: 'YYYY-MM-DD', lang: 'es'});
			toDate.reloadOptions({ minDate: '1981-01-01', maxDate: moment(), format: 'YYYY-MM-DD', lang: 'es'});
		} else if (val === 'ETo') {
			fromDate.reloadOptions({ minDate: '2000-01-01', maxDate: moment(), format: 'YYYY-MM-DD', lang: 'es'});
			toDate.reloadOptions({ minDate: '2000-01-01', maxDate: moment(), format: 'YYYY-MM-DD', lang: 'es'});
		} else {
			fromDate.reloadOptions({ minDate: '2000-01-01', maxDate: moment(), format: 'YYYY-MM-DD', lang: 'es'});
			toDate.reloadOptions({ minDate: '2000-01-01', maxDate: moment(), format: 'YYYY-MM-DD', lang: 'es'});
		}

	});

	$("#searchBtn").click(function() {
		var idstore = [];
		map.eachLayer(function(layer){
		    console.log(layer);
		});
		$('#chart').html('');
		$('#chartToolbar').html('');
		var fd = fromDate.getDate();
		var td = toDate.getDate();
		var variable = $("#variable").val();

		if (variable === '') {
			alert('Por favor seleccione un valor para la variable');
			return;
		}

		if (fd == null || td == null) {
			alert('Por favor verifique el valor de las fechas');
			return;
		} else {
			if (td.isBefore(fd)) {
				alert('Por favor verifique que la fecha inicial sea mayor que la fecha final');
				return;
			}
		}

		showhideLoading(true);
		
		var params = {
			dateStart: [fd.year(), fd.month()+1, fd.date()],
			dateEnd: [td.year(), td.month()+1, td.date()],
			variable: variable
		}

		//validar parametros de consulta antes de enviarla 
		$("#searchBtn").prop("disabled", true);
		$.ajax({
			type: "POST",
			url: "/gee/map",
			// The key needs to match your method's input parameter (case-sensitive).
			data: JSON.stringify(params),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function(data) {
				addLayer(data, params);
			},
			failure: function(errMsg) {
				alert('Ocurrio un error');
			}
		}).always(function() {
			$("#searchBtn").prop("disabled", false);
			showhideLoading(false);
		});

	});

	var geeLayer = null;
	function addLayer(data, params) {
		if (data.error) {
			alert(data.msg);
			return;
		}

		//limpiar la capa actual de gee
		if (geeLayer != null) {
			geeLayer.remove();
			geeLayer = null;
		};

		//agregar la capa de google earth engine
		geeLayer = L.tileLayer('https://earthengine.googleapis.com/map/{id}/{z}/{x}/{y}?token={accessToken}', {
			maxZoom: 18,
			id: data.mapid,
			accessToken: data.token
		}).addTo(map);

		
		var key = data.mapid+'_'+data.token;
/*		createChart(key, data.chart);*/
		

		//agregar marker al mapa
		var bbox = data.bounds;

		var bounds = L.latLngBounds(L.latLng(bbox[1], bbox[0]), L.latLng(bbox[3], bbox[2]));
		var bCenter = bounds.getCenter();
		map.fitBounds(bounds);
		var ll = [bCenter.lat, bCenter.lng];
		if (marker != null) {
			map.removeLayer(marker); 
		}

	    // create popup contents
	    var customPopup = "<div style='text-align:center;'><div id='chart'></div><div id='chartToolbar' style='margin-top:10px;'></div></div>";
	    
	    // specify popup options 
	    var customOptions =
	        {
	        'maxWidth': '500',
	        'className' : 'custom'
	        }

		marker = L.marker(ll, {draggable:'true'});//.bindPopup("<b>Hello world!</b><br />I am a popup.").openPopup();
		marker.bindPopup(customPopup,customOptions);
		marker.addTo(map);
		marker.bindTooltip("Mueva este marcador para obtener el valor en un punto del mapa").openTooltip();
		marker.on('dragstart', function(){
			marker.bindTooltip("Mueva este marcador para obtener el valor en un punto del mapa").openTooltip();
		});
		marker.on('dragend', function(event){
			$('#chart').html('');
			this.openPopup();
			var marker = event.target;
			var position = marker.getLatLng();
			if (!bounds.contains(position)) {
				marker.bindTooltip('El punto esta fuera de la zona de consulta').openTooltip();
			} else {
				params.lon = position.lng;
				params.lat = position.lat;
				showhideLoading(true);
				pointValue(params,this);
			}
		});

	};
	

	function pointValue(params,marker) {
		$.ajax({
			type: "POST",
			url: "/gee/map/value",
			// The key needs to match your method's input parameter (case-sensitive).
			data: JSON.stringify(params),
			contentType: "application/json; charset=utf-8",
			dataType: "json",
			success: function(data) {
				if (data.error) {
					alert(data.msg);
					return;    
				}

				if (data.properties && data.properties.value) {
					var value = ""+data.properties.value;
					if (marker != null) {
						marker.bindTooltip(value).openTooltip();
					}
				}


				createChart(guid(), data.chart, marker);
			},
			failure: function(errMsg) {
				alert('Ocurrio un error');
			}
		}).always(function() {
			showhideLoading(false);
		});
	}

	function showhideLoading(show) {
		if (show) {
			$("#loading").show();
			//$(".leaflet-popup-content-wrapper").show();
			
		} else {
			$("#loading").hide();
			//$(".leaflet-popup-content-wrapper").hide();

		}

	};

	showhideLoading(false);

	checkBrowser();
});



function createChart(key, chart, marker) {
	if (chart) {
		csv[key] = chart.data;
		$('#chart').html('');
		var chartData = google.visualization.arrayToDataTable(chart.data);
		var gchart;
		if (chart.type === 'series') {
			gchart = new google.visualization.LineChart(document.getElementById("chart"));
		} else {
			chartData = new google.visualization.DataView(chartData);
			gchart = new google.visualization.ColumnChart(document.getElementById("chart"));
		}

		console.log(chartData);
		console.log(gchart);

		google.visualization.events.addListener(gchart, 'ready', function() {
			$('#chartToolbar').html('<a href="' + gchart.getImageURI() + '" download="chart.png">Exportar PNG</a>');
			$('#chartToolbar').append('<a style="margin-left:20px" href="#" onclick="(exportCSV(\''+key+'\'))">Exportar CSV</a>');
		});
		gchart.draw(chartData, chart.options);
		//marker.openPopup();
	}
}

function exportPNG(img) {
	var url = img.replace(/^data:image\/[^;]+/, 'data:application/octet-stream');
	window.open(url);
}

function exportCSV(key) {
	var data = csv[key];
	var header = data[0];
	var rowH = [];
	for (var i = 0; i<header.length; i++) {
		var h = header[i];
		if (typeof h === 'object') {
			rowH.push('"'+h.label+"'");
		} else {
			rowH.push('"'+h+'"');
		}
	}
	
	var dataTable = google.visualization.arrayToDataTable(data);
    var text = google.visualization.dataTableToCsv(dataTable);
    var csvContent = "data:text/csv;charset=utf-8,"+rowH.join(',')+"\n\r"+text;

	var encodedUri = encodeURI(csvContent);
    var link = document.createElement("a");
    link.setAttribute("href", encodedUri);
    link.setAttribute("download", "chart.csv");
    document.body.appendChild(link); // Required for FF
    link.click();
    document.body.removeChild(link); 

}


function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}



function checkBrowser() {
	// please note, 
	// that IE11 now returns undefined again for window.chrome
	// and new Opera 30 outputs true for window.chrome
	// and new IE Edge outputs to true now for window.chrome
	// and if not iOS Chrome check
	// so use the below updated condition
	var isChromium = window.chrome;
	var winNav = window.navigator;
	var vendorName = winNav.vendor;
	var isOpera = typeof window.opr !== "undefined";
	var isIEedge = winNav.userAgent.indexOf("Edge") > -1;
	var isIOSChrome = winNav.userAgent.match("CriOS");

	if(isIOSChrome){
	   console.log('is Google Chrome on IOS');
	   alert('is Google Chrome on IOS');
	} else if (
	  isChromium !== null &&
	  typeof isChromium !== "undefined" &&
	  vendorName === "Google Inc." &&
	  isOpera === false &&
	  isIEedge === false
	) {
	  //console.log('is Google Chrome');
	  //alert('is Google Chrome');
	} else { 
	   //console.log('not Google Chrome');
	   alert('IMPORTANTE: Esta utilizando un navegador diferente a Google Chrome. Este sitio web trabaja mejor con Google Chrome y puede que no trabaje con otros navegadores. Por favor intente abrir de nuevo esta pagina con Google Chrome.'); 
	}
};

  function degToDMS(decDeg, decDir) {
    /** @type {number} */
    var d = Math.abs(decDeg);
    /** @type {number} */
    var deg = Math.floor(d);
    d = d - deg;
    /** @type {number} */
    var min = Math.floor(d * 60);
    /** @type {number} */
    var sec = Math.floor((d - min / 60) * 60 * 60);
    if (sec === 60) { // can happen due to rounding above
      min++;
      sec = 0;
    }
    if (min === 60) { // can happen due to rounding above
      deg++;
      min = 0;
    }
    /** @type {string} */
    var min_string = min < 10 ? "0" + min : min;
    /** @type {string} */
    var sec_string = sec < 10 ? "0" + sec : sec;
    /** @type {string} */
    var dir = (decDir === 'LAT') ? (decDeg < 0 ? "S" : "N") : (decDeg < 0 ? "W" : "E");

    return (decDir === 'LAT') ?
      deg + "&deg;" + min_string + "&prime;" + sec_string + "&Prime;" + dir :
      deg + "&deg;" + min_string + "&prime;" + sec_string + "&Prime;" + dir;
  };

/*        _redondearNumero: function(numero){
        var formatNumber = {
         separador: ".", // separador para los miles
         sepDecimal: ',', // separador para los decimales
         formatear:function (num){
         num +='';
         var splitStr = num.split('.');
         var splitLeft = splitStr[0];
         var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
         var regx = /(\d+)(\d{3})/;
         while (regx.test(splitLeft)) {
         splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
         }
         return this.simbol + splitLeft +splitRight;
         },
         new:function(num, simbol){
         this.simbol = simbol ||'';
         return this.formatear(num);
         }
        }
        var res = formatNumber.new(numero.toFixed(2));     
        return res;
      }, */