/**
 * importar libreria requeridas
 */
const ee = require('@google/earthengine');
const express = require('express');
const handlebars = require('express-handlebars');

/**
 * definir servidor
 */
const app = express()
  .engine('.hbs', handlebars({extname: '.hbs', cache: false}))
  .set('view engine', '.hbs')
  .use(express.static('static'))
  .use('/gee/static', express.static('static'));

app.use(express.json());


/**
 * definicion de constantes
 */
const bounds = [-73.05, 10.97, -72.66, 10.83]


/**
 * definicion de rutas
 */
 app.get('/gee', (request, response) => {
    response.render('index', {});
});

app.post('/gee/map/value', (request, response) => {
    queryData(request, response, true);
});

app.post('/gee/map', (request, response) => {
    queryData(request, response, false);
});   


/**
 * funcion para consultar la informacion de GEE
 */
function queryData(request, response, isPointValue) {
    var params = request.body;
    var variable = params.variable;

    var values = {
        dateStart: params.dateStart,
        dateEnd: params.dateEnd,
        aoi: ee.Geometry.Rectangle(bounds[0], bounds[1], bounds[2], bounds[3]),
        isPointValue: isPointValue
    };

    if (isPointValue) {
        values.lon = params.lon;
        values.lat = params.lat;
    }
      
    if (variable === 'NDVI-LANDSAT') {
        ndviLandsat(values, request, response);
    } else if (variable === 'NDVI-SENTINEL') {
        ndviSentinel(values, request, response);
    } else if (variable === 'PRECIPITATION') {
        precipitation(values, request, response);
    } else if (variable === 'ETo') {
        et0(values, request, response);

	} else if (variable === 'ET_MODIS') {
		etModis(values, request, response);
	} else if (variable === 'PRECIPITACION_DAILY') {
		precipitationDaily(values, request, response);
	} else if (variable === 'SAVI_LANDSAT') {
		saviLandsat(values, request, response);
	} else if (variable === 'SAVI_SENTINEL') {
		saviSentinel(values, request, response);

    } else {
        //retornar error
        response.send({error: true, msg: 'Variable desconocida'}); 
    }
}


/**
 * funcion para consultar la informacion de ndvi en lansat
 */
function ndviLandsat(values, request, response) {
    console.log('sis');
    var l8 = ee.ImageCollection("LANDSAT/LC08/C01/T1_TOA");
    var startDate = ee.Date.fromYMD(values.dateStart[0], values.dateStart[1], values.dateStart[2]);
    var endDate = ee.Date.fromYMD(values.dateEnd[0], values.dateEnd[1], values.dateEnd[2]);
    // set cloud threshold
    var cloud_thresh = 30;

    var cloudfunction = function(image) {
        //use add the cloud likelihood band to the image
        var CloudScore = ee.Algorithms.Landsat.simpleCloudScore(image);
        //isolate the cloud likelihood band
        var quality = CloudScore.select('cloud');
        //get pixels above the threshold
        var cloud01 = quality.gt(cloud_thresh);
        //create a mask from high likelihood pixels
        var cloudmask = image.mask().and(cloud01.not());
        //mask those pixels from the image
        return image.updateMask(cloudmask);
    };

    // filter on date and location
    var l8images = l8.filterDate(startDate, endDate).map(cloudfunction).filterBounds(values.aoi);
 
    var ndvi = l8images.map(function(image) {
        return image.normalizedDifference(['B5', 'B4'])
                    .set('system:time_start', image.get('system:time_start'));
    });

    if (values.isPointValue) {
        var point = ee.Geometry.Point(values.lon, values.lat);
        var features = ndvi.filterBounds(point).map(function(image){
            var nd = image.reduceRegion(ee.Reducer.mean(), point, 30).get('nd');
            return ee.Feature(null, {
				'date': ee.Date(image.get('system:time_start')).millis(), 
                'system:time_start': ee.Date(image.get('system:time_start')).format('yyyy MM dd'),
                'nd': nd
            });
        }).filter(ee.Filter.neq('nd',null));
        

        var chartData = features.getInfo().features.map(f => {
			var d = new Date(f['properties']['date']);
            return [
                //"Date("+f['properties']['system:time_start'].split(" ").join(",")+")",
				"Date("+d.getFullYear()+","+d.getMonth()+","+ d.getDate()+","+d.getHours()+","+
					    d.getMinutes()+","+d.getSeconds()+","+d.getMilliseconds()+")",
                f['properties']['nd']
            ];
        });

        if (chartData.length == 0) {
            response.send({error: true, msg: 'No hay datos disponibles para el rango de fechas seleccionado.'}); 
            return;
	    }

        console.log(chartData);
        chartData.sort(function(a, b){
            var x = a[0].toLowerCase();
            var y = b[0].toLowerCase();
            x = x.replace("date(","");
            x = x.replace(")",""); 
            y = y.replace("date(","");
            y = y.replace(")","");
            x = x.split(',');
            y = y.split(',');
            var burbuja = 0;
            for (let i = 0; i < x.length; i++){
                console.log(x[i]+"  "+y[i]);     
                if (parseInt(x[i]) < parseInt(y[i])) {               
                 console.log("menor");
                 burbuja = -1;
                 break;
                }
                if (parseInt(x[i]) > parseInt(y[i])) {
                 console.log("mayor");
                 burbuja = 1;
                 break;
                }
            }
            return burbuja;


/*            console.log(a[0].length);
            console.log(b[0]);
            if (x < y) {return -1;}
            if (x > y) {return 1;}
            return 0;*/
        }); 

        console.log('mecha');
        console.log(chartData);

        chartData = [[{type: 'date', label: 'time'}, 'NDVI']].concat(chartData);
        var chartOptions = { title: 'NDVI', hAxis: {title: 'Fecha'}, vAxis: {title: 'NDVI'} };

        response.send({chart: { type: 'series', options: chartOptions, data: chartData }});
        return;

    }

    var image = ndvi.median().clip(values.aoi);
    var pViz = {min: -1, max: 2, palette: ['black', 'ff0004', '0000ff', '#cccc66', '003f15','#9cab68','6bf442', 'f402ec']};
    image.getMap(pViz, ({mapid, token}) => {
        response.send({
            mapid : mapid, 
            token: token,
            min: pViz.min,
            max: pViz.max,
            bounds: bounds
        }); 
    });
}


function ndviSentinel(values, request, response) {
    var startDate = ee.Date.fromYMD(values.dateStart[0], values.dateStart[1], values.dateStart[2]);
    var endDate = ee.Date.fromYMD(values.dateEnd[0], values.dateEnd[1], values.dateEnd[2]);

    function maskS2clouds(image) {
        var qa = image.select('QA60');
        // Bits 10 and 11 are clouds and cirrus, respectively.
        var cloudBitMask = ee.Number(2).pow(10).int();
        var cirrusBitMask = ee.Number(2).pow(11).int();

        // Both flags should be set to zero, indicating clear conditions.
        var mask = qa.bitwiseAnd(cloudBitMask).eq(0).and(
                    qa.bitwiseAnd(cirrusBitMask).eq(0));

        // Return the masked and scaled data, without the QA bands.
        return image.updateMask(mask).divide(10000).select("B.*")
            .copyProperties(image, ["system:time_start"]);
    }

    // Map the function over one year of data and take the median.
    // Load Sentinel-2 TOA reflectance data.
    var collection = ee.ImageCollection('COPERNICUS/S2')
            .filterDate(startDate, endDate)
            // Pre-filter to get less cloudy granules.
            .filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', 20))
            .map(maskS2clouds);

    var ndvi = collection.map(function(image) {
        return image.normalizedDifference(['B8', 'B4'])
            .set('system:time_start', image.get('system:time_start'));
    });

    // en caso que la consulta sea un punto.
    if (values.isPointValue) {
        var point = ee.Geometry.Point(values.lon, values.lat);
        //point = ee.Geometry.Point(-73.0125886135286, 10.92988879690451);

        var features = ndvi.filterBounds(point).map(function(image){
            var nd = image.reduceRegion(ee.Reducer.mean(), point, 30).get('nd');
            return ee.Feature(null, {
				'date': ee.Date(image.get('system:time_start')).millis(), 
                'system:time_start': ee.Date(image.get('system:time_start')).format('yyyy MM dd'),
                'nd': nd
            });
        }).filter(ee.Filter.neq('nd',null));
        
        var chartData = features.getInfo().features.map(f => {
			var d = new Date(f['properties']['date']);
            return [
                //"Date("+f['properties']['system:time_start'].split(" ").join(",")+")",
				"Date("+d.getFullYear()+","+d.getMonth()+","+ d.getDate()+","+d.getHours()+","+
					    d.getMinutes()+","+d.getSeconds()+","+d.getMilliseconds()+")",
                f['properties']['nd']
            ];
        });

        if (chartData.length == 0) {
            response.send({error: true, msg: 'No hay datos disponibles para el rango de fechas seleccionado.'}); 
            return;
	    }

        chartData = [[{type: 'date', label: 'time'}, 'NDVI']].concat(chartData);
        var chartOptions = { title: 'NDVI', hAxis: {title: 'Fecha'}, vAxis: {title: 'NDVI'} };

        response.send({chart: { type: 'series', options: chartOptions, data: chartData }});
        return;
    }

    //generar las imagenes del mapa
    var image = ndvi.mean().clip(values.aoi);
    var pViz = {min: -1, max: 2, palette: ['black', 'ff0004', '0000ff', '#cccc66', 
        '003f15','#9cab68','6bf442', 'f402ec']};
    
    var chartOptions = {};
    var chartData = {};
    var chart; //{ options: chartOptions, data: chartData }

    image.getMap(pViz, ({mapid, token}) => {
        response.send({
            chart: chart,
            mapid : mapid, 
            token: token,
            min: pViz.min,
            max: pViz.max,
            bounds: bounds
        }); 
    });
}



/**
 * funcion para consultar la precipitacion
 */
function precipitation(values, request, response) {
    var chirps = ee.ImageCollection("UCSB-CHG/CHIRPS/DAILY");
    
    var startDate = ee.Date.fromYMD(values.dateStart[0], values.dateStart[1], values.dateStart[2]);
    var endDate = ee.Date.fromYMD(values.dateEnd[0], values.dateEnd[1], values.dateEnd[2]);

    var startYear = values.dateStart[0]; 
    var endYear = values.dateEnd[0];
    
    var years = ee.List.sequence(startYear, endYear);
    var months = ee.List.sequence(1, 12);   

    var monthlyPrecip = ee.ImageCollection.fromImages(
        years.map(function (year) {
            return months.map(function(m) {
                var a = chirps.filter(ee.Filter.calendarRange(year, year, 'year'))
                          .filter(ee.Filter.calendarRange(m, m, 'month'))
                          .sum();

                return a.set('year', year)
                        .set('month', m)
                        .set('system:time_start', ee.Date.fromYMD(year, m, 1));
            });
        }).flatten()
    );
 /*   if (values.isPointValue) {
        try {
           var point = ee.Geometry.Point(values.lon, values.lat);
            var filtered = monthlyPrecip.filterBounds(point).filterDate(startDate, endDate).mean();
            console.log(filtered.toString());
            var reduction = monthlyPrecip.reduceRegion(ee.Reducer.mean(), point, 2500);
            var feature = ee.Feature(point, { 'value' : reduction.get('precipitation') });
            response.send(feature.getInfo()); 

            return;
        }
        catch(error) {
          console.error("error precipitation "+error);
        }
    }*/

    function computeMean(img){
        if (values.isPointValue) {
            var point = ee.Geometry.Point(values.lon, values.lat);
            var reduction = img.reduceRegion(ee.Reducer.mean(), point, 2500);
        }
        else{
            var reduction = img.reduceRegion(ee.Reducer.mean(), values.aoi, 2500);
        }
        return ee.Feature(null, {
            'precipitation': reduction.get('precipitation'),
            'system:time_start': ee.Date(img.get('system:time_start')).format('MMM yyyy')
        });
    };
    
    var features = monthlyPrecip.map(computeMean).getInfo();
    var chartData = features.features.map(f => {
        return [
            f['properties']['system:time_start'],
            f['properties']['precipitation']
        ];
    });

    if (chartData.length == 0) {
         response.send({error: true, msg: 'No hay datos disponibles para el rango de fechas seleccionado.'}); 
         return;
    }

    chartData = [['time', 'precipitation']].concat(chartData);


    var chartOptions = {
        title: 'Precipitacion media mensual',
        hAxis: {title: 'Tiempo'},
        vAxis: {title: 'Precipitacion (mm)'}
    };
   
    var image = monthlyPrecip.mean().clip(values.aoi);
    var pViz = {
		min: 0, 
		max: 300, 
		palette: '000000, 0000FF, FDFF92, FF2700, FF00E7'
    };
    
    image.getMap(pViz, ({mapid, token}) => {
        response.send({
            mapid : mapid, 
            token: token,
            min: pViz.min,
            max: pViz.max,
            bounds: bounds,
            chart: { options: chartOptions, data: chartData }
        }); 
    });  
}


/**
 * funcion para consultar la ETo
 */
function et0(values, request, response) {
    var mod16 = ee.ImageCollection("MODIS/NTSG/MOD16A2/105");
	var mod16a = ee.ImageCollection("MODIS/006/MOD16A2");

    var startDate = ee.Date.fromYMD(values.dateStart[0], values.dateStart[1], values.dateStart[2]);
    var endDate = ee.Date.fromYMD(values.dateEnd[0], values.dateEnd[1], values.dateEnd[2]);
    
    var startYear = values.dateStart[0]; 
    var endYear = values.dateEnd[0];

    var years = ee.List.sequence(startYear, endYear);
    var months = ee.List.sequence(1, 12);

    var ET = mod16a.select("ET");

    var monthlyET =  ee.ImageCollection.fromImages(
        years.map(function (y) {
            return months.map(function(m) {
            var w = ET.filter(ee.Filter.calendarRange(y, y, 'year'))
                        .filter(ee.Filter.calendarRange(m, m, 'month'))
                        .sum().multiply(0.1);
            return w.set('year', y)
                    .set('month', m)
                    .set('system:time_start', ee.Date.fromYMD(y, m, 1));
                            
            });
        }).flatten()
    );

    /*
    if (values.isPointValue) {
        var point = ee.Geometry.Point(values.lon, values.lat);
        var filtered = monthlyET.filterBounds(point).filterDate(startDate, endDate).mean();
        var reduction = filtered.reduceRegion(ee.Reducer.mean(), point, 2500);
        var feature = ee.Feature(point, { 'value': reduction.get('ET') });
        response.send(feature.getInfo()); 
        return;
    }
*/


    var meanMonthlyET =  ee.ImageCollection.fromImages(
        months.map(function (m) {
        var w = monthlyET.filter(ee.Filter.eq('month', m)).mean().clip(values.aoi);
            return w.set('month', m)
                    .set('system:time_start',ee.Date.fromYMD(1, m, 1)); 
        }).flatten()
    );
    
    function computeMean(img){
        if (values.isPointValue) {
            var point = ee.Geometry.Point(values.lon, values.lat);
            var reduction = img.reduceRegion(ee.Reducer.mean(), point, 2500);
        }
        else{
            var reduction = img.reduceRegion(ee.Reducer.mean(), values.aoi, 2500);
        }
        return ee.Feature(null, {
            'ET': reduction.get('ET'),
            'system:time_start': ee.Date(img.get('system:time_start')).format('MMM')
        });
    };
    
    var features = meanMonthlyET.map(computeMean).getInfo();
    var chartData = features.features.map(f => {
        return [
            f['properties']['system:time_start'],
            f['properties']['ET']
        ];
    });

    if (chartData.length == 0) {
         response.send({error: true, msg: 'No hay datos disponibles para el rango de fechas seleccionado.'}); 
         return;
    }
    chartData = [['time', 'eto']].concat(chartData);

    var chartOptions = {
        title: 'Evapotranspiracion mensual multianual ET',
        hAxis: {title: 'Tiempo'},
        vAxis: {title: 'ET (mm)'}
    };

    
    var pViz = { min: 0, 
				 max: 300, 
				 palette: ' fff5d1, bfff9e, 50ffae,38acff,1f45ff,091cff'  
			};
    meanMonthlyET.getMap(pViz, ({mapid, token}) => {
        response.send({
            mapid : mapid, 
            token: token,
            min: pViz.min,
            max: pViz.max,
            bounds: bounds,
            chart: { options: chartOptions, data: chartData }
        }); 
    }); 
   
}



function etModis(values, request, response) {
	var startDate = ee.Date.fromYMD(values.dateStart[0], values.dateStart[1], values.dateStart[2]);
    var endDate = ee.Date.fromYMD(values.dateEnd[0], values.dateEnd[1], values.dateEnd[2]);

    var mod16a = ee.ImageCollection("MODIS/006/MOD16A2");
	var ET = mod16a.select("ET");
	
   // en caso que la consulta sea un punto.
    if (values.isPointValue) {
        var point = ee.Geometry.Point(values.lon, values.lat);
        //point = ee.Geometry.Point(-73.05, 10.97);
		var ETimages = ET.filterDate(startDate,endDate).filterBounds(point);


        var features = ETimages.map(function(image){
            var nd = image.reduceRegion(ee.Reducer.mean(), point, 30).get('ET');
            return ee.Feature(null, {
				'date': ee.Date(image.get('system:time_start')).millis(), 
                'system:time_start': ee.Date(image.get('system:time_start')).format('yyyy MM dd'),
                'nd': nd
            });
        }).filter(ee.Filter.neq('nd',null));
        
        var chartData = features.getInfo().features.map(f => {
			var d = new Date(f['properties']['date']);
            return [
                //"Date("+f['properties']['system:time_start'].split(" ").join(",")+")",
				"Date("+d.getFullYear()+","+d.getMonth()+","+ d.getDate()+","+d.getHours()+","+
					    d.getMinutes()+","+d.getSeconds()+","+d.getMilliseconds()+")",
                f['properties']['nd']
            ];
        });

        if (chartData.length == 0) {
            response.send({error: true, msg: 'No hay datos disponibles para el rango de fechas seleccionado.'}); 
            return;
	    }

        chartData = [[{type: 'date', label: 'time'}, 'ET']].concat(chartData);
        var chartOptions = { title: 'ET', hAxis: {title: 'Fecha'}, vAxis: {title: 'ET'} };

        response.send({chart: { type: 'series', options: chartOptions, data: chartData }});
        return;
    }

    //generar las imagenes del mapa
	var ETimages = ET.filterDate(startDate,endDate).filterBounds(values.aoi);
	var image = ETimages.mean().clip(values.aoi);
    
	
	var pViz = {
		min: 0, 
		max: 300, 
		palette: ' fff5d1, bfff9e, 50ffae,38acff,1f45ff,091cff' 
	};
    
    
    var chart;

    image.getMap(pViz, ({mapid, token}) => {
        response.send({
            chart: chart,
            mapid : mapid, 
            token: token,
            min: pViz.min,
            max: pViz.max,
            bounds: bounds
        }); 
    });



}

function precipitationDaily(values, request, response) {
	var startDate = ee.Date.fromYMD(values.dateStart[0], values.dateStart[1], values.dateStart[2]);
    var endDate = ee.Date.fromYMD(values.dateEnd[0], values.dateEnd[1], values.dateEnd[2]);
	var chirps = ee.ImageCollection("UCSB-CHG/CHIRPS/DAILY");

	// en caso que la consulta sea un punto.
    if (values.isPointValue) {
        var point = ee.Geometry.Point(values.lon, values.lat);
		var Pimages = chirps.filterDate(startDate,endDate).filterBounds(point);

        var features = Pimages.map(function(image){
            var nd = image.reduceRegion(ee.Reducer.mean(), point, 30).get('precipitation');
            return ee.Feature(null, {
				'date': ee.Date(image.get('system:time_start')).millis(), 
                'system:time_start': ee.Date(image.get('system:time_start')).format('yyyy MM dd'),
                'nd': nd
            });
        }).filter(ee.Filter.neq('nd',null));
        
        var chartData = features.getInfo().features.map(f => {
			var d = new Date(f['properties']['date']);
            return [
				"Date("+d.getFullYear()+","+d.getMonth()+","+ (d.getDate()+1)+","+d.getHours()+","+
					    d.getMinutes()+","+d.getSeconds()+","+d.getMilliseconds()+")",
                f['properties']['nd']
            ];
        });

        if (chartData.length == 0) {
            response.send({error: true, msg: 'No hay datos disponibles para el rango de fechas seleccionado.'}); 
            return;
	    }

        chartData = [[{type: 'date', label: 'time'}, 'precipitation']].concat(chartData);
        var chartOptions = { title: 'precipitation', hAxis: {title: 'Fecha'}, vAxis: {title: 'precipitation'} };

        response.send({chart: { type: 'series', options: chartOptions, data: chartData }});
        return;
    }

    //generar las imagenes del mapa
	var Pimages = chirps.filterDate(startDate,endDate).filterBounds(values.aoi);
	var image = Pimages.mean().clip(values.aoi);
    
	var pViz = {
		min: 0, 
		max: 30, 
		palette:'000000, 0000FF, FDFF92, FF2700, FF00E7'
	};
    
    var chart;

    image.getMap(pViz, ({mapid, token}) => {
        response.send({
            chart: chart,
            mapid : mapid, 
            token: token,
            min: pViz.min,
            max: pViz.max,
            bounds: bounds
        }); 
    });

}

function saviLandsat(values, request, response) {
	var startDate = ee.Date.fromYMD(values.dateStart[0], values.dateStart[1], values.dateStart[2]);
    var endDate = ee.Date.fromYMD(values.dateEnd[0], values.dateEnd[1], values.dateEnd[2]);
	var l8 = ee.ImageCollection("LANDSAT/LC08/C01/T1_TOA");
	
	// set cloud threshold
	var cloud_thresh = values.isPointValue ? 40 : 30;
 
	// the cloud function
	var cloudfunction = function(image) {
		//use add the cloud likelihood band to the image
		var CloudScore = ee.Algorithms.Landsat.simpleCloudScore(image);
		//isolate the cloud likelihood band
		var quality = CloudScore.select('cloud');
		//get pixels above the threshold
		var cloud01 = quality.gt(cloud_thresh);
		//create a mask from high likelihood pixels
		var cloudmask = image.mask().and(cloud01.not());
		//mask those pixels from the image
		return image.updateMask(cloudmask);
	};

	// en caso que la consulta sea un punto.
    if (values.isPointValue) {
		var point = ee.Geometry.Point(values.lon, values.lat);
		//point = ee.Geometry.Point(-73.05, 10.97);
		var l8images = l8.filterDate(startDate,endDate).filterBounds(point);
		l8images = l8images.map(cloudfunction);

		var savi = l8images.map(function(image) {
			return image.expression(
				('1.5 * ((NIR - RED) / (NIR + RED + 0.5))'),
				{'NIR': image.select('B5'),'RED': image.select('B4')}).set('system:time_start', image.get('system:time_start'));      
		});

	
		var features = savi.map(function(image){
            var nd = image.reduceRegion(ee.Reducer.mean(), point, 30).get('constant');
            return ee.Feature(null, {
				'date': ee.Date(image.get('system:time_start')).millis(), 
                'system:time_start': ee.Date(image.get('system:time_start')).format('yyyy MM dd'),
                'nd': nd
            });
        }).filter(ee.Filter.neq('nd',null));
        
        var chartData = features.getInfo().features.map(f => {
			var d = new Date(f['properties']['date']);
            return [
				f['properties']['date'],
				"Date("+d.getFullYear()+","+d.getMonth()+","+d.getDate()+","+d.getHours()+","+
					    d.getMinutes()+","+d.getSeconds()+","+d.getMilliseconds()+")",
                f['properties']['nd']
            ];
        });

        if (chartData.length == 0) {
            response.send({error: true, msg: 'No hay datos disponibles para el rango de fechas seleccionado.'}); 
            return;
	    }

		chartData.sort(function(a, b){
			var x = a[0];
            var y = b[0];
            if (x < y) {return -1;}
            if (x > y) {return 1;}
            return 0;
        }); 

		chartData = chartData.map(f => {
			 return [f[1],  f[2]];
		});
		
		chartData = [[{type: 'date', label: 'time'}, 'constant']].concat(chartData);
        var chartOptions = { title: 'constant', hAxis: {title: 'Fecha'}, vAxis: {title: 'constant'} };

        response.send({chart: { type: 'series', options: chartOptions, data: chartData }});
        return;

	}


	var l8images = l8.filterDate(startDate,endDate).filterBounds(values.aoi);
	// filter clouds for all images in imagecollection
	l8images = l8images.map(cloudfunction);
	
	var savi = l8images.map(function(image) {
		return image.expression(
			('1.5 * ((NIR - RED) / (NIR + RED + 0.5))'),
			{'NIR': image.select('B5'),'RED': image.select('B4')}).set('system:time_start', image.get('system:time_start'));      
		});


	var image = savi.mean().clip(values.aoi);
    
	var pViz = {
		min: -1, 
		max: 2, 
		palette: ['black', 'ff0004', '0000ff', '#cccc66', '003f15','#9cab68','6bf442', 'f402ec']
	};
    
    var chart;

	image.getMap(pViz, ({mapid, token}) => {
        response.send({
            chart: chart,
            mapid : mapid, 
            token: token,
            min: pViz.min,
            max: pViz.max,
            bounds: bounds
        }); 
    });

}

function saviSentinel(values, request, response) {
	var startDate = ee.Date.fromYMD(values.dateStart[0], values.dateStart[1], values.dateStart[2]);
    var endDate = ee.Date.fromYMD(values.dateEnd[0], values.dateEnd[1], values.dateEnd[2]);

	function maskS2clouds(image) {
		var qa = image.select('QA60')
		// Bits 10 and 11 are clouds and cirrus, respectively.
		var cloudBitMask = ee.Number(2).pow(10).int()
		var cirrusBitMask = ee.Number(2).pow(11).int()
		// Both flags should be set to zero, indicating clear conditions.
		var mask = qa.bitwiseAnd(cloudBitMask).eq(0).and(
				 qa.bitwiseAnd(cirrusBitMask).eq(0))
		// Return the masked and scaled data, without the QA bands.
		return image.updateMask(mask).divide(10000)
			.select("B.*").copyProperties(image, ["system:time_start"])
	}

	// Map the function over one year of data and take the median.
	// Load Sentinel-2 TOA reflectance data.
	var collection = ee.ImageCollection('COPERNICUS/S2')
		.filterDate(startDate, endDate)
		// Pre-filter to get less cloudy granules.
		.filter(ee.Filter.lt('CLOUDY_PIXEL_PERCENTAGE', 20))
		.map(maskS2clouds);

	var savi = collection.map(function(image) {
		return image.expression(
			('1.5 * ((NIR - RED) / (NIR + RED + 0.5))'),
			{'NIR': image.select('B8'),'RED': image.select('B4')}).set('system:time_start', image.get('system:time_start'));      
	});


	// en caso que la consulta sea un punto.
    if (values.isPointValue) {
		var point = ee.Geometry.Point(values.lon, values.lat);
		//point = ee.Geometry.Point(-73.05, 10.97);
		var savi1 = savi.filterDate(startDate,endDate).filterBounds(point);

		var features = savi1.map(function(image){
            var nd = image.reduceRegion(ee.Reducer.mean(), point, 30).get('constant');
            return ee.Feature(null, {
				'date': ee.Date(image.get('system:time_start')).millis(), 
                'system:time_start': ee.Date(image.get('system:time_start')).format('yyyy MM dd'),
                'nd': nd
            });
        }).filter(ee.Filter.neq('nd',null));
        
        var chartData = features.getInfo().features.map(f => {
			var d = new Date(f['properties']['date']);
            return [
				f['properties']['date'],
				"Date("+d.getFullYear()+","+d.getMonth()+","+d.getDate()+","+d.getHours()+","+
					    d.getMinutes()+","+d.getSeconds()+","+d.getMilliseconds()+")",
                f['properties']['nd']
            ];
        });

        if (chartData.length == 0) {
            response.send({error: true, msg: 'No hay datos disponibles para el rango de fechas seleccionado.'}); 
            return;
	    }

		chartData.sort(function(a, b){
			var x = a[0];
            var y = b[0];
            if (x < y) {return -1;}
            if (x > y) {return 1;}
            return 0;
        }); 

		chartData = chartData.map(f => {
			 return [f[1],  f[2]];
		});
		
		chartData = [[{type: 'date', label: 'time'}, 'constant']].concat(chartData);
        var chartOptions = { title: 'constant', hAxis: {title: 'Fecha'}, vAxis: {title: 'constant'} };

        response.send({chart: { type: 'series', options: chartOptions, data: chartData }});
        return;

	}



	var image = savi.mean().clip(values.aoi);
	var pViz = {
		min: -1, 
		max: 2,
		palette: ['black', 'ff0004', '0000ff', '#cccc66', '003f15','#9cab68','6bf442', 'f402ec']
	};
    
    var chart;
	image.getMap(pViz, ({mapid, token}) => {
        response.send({
            chart: chart,
            mapid : mapid, 
            token: token,
            min: pViz.min,
            max: pViz.max,
            bounds: bounds
        }); 
    });



}







/**
 * definicion de acceso y autenticacion a GEE
 */
/*const PRIVATE_KEY = require('./gearth-210619-d70adabc5e28.json');
const PORT = process.env.PORT || 3031;*/


/**
 * autenticar en GEE e iniciar el servidor
 */
/*ee.data.authenticateViaPrivateKey(PRIVATE_KEY, () => {
    ee.initialize(null, null, () => {
        app.listen(PORT);
        console.log(`Listening on port ${PORT}`);
    });
});*/

/**
 * definicion de acceso y autenticacion a GEE
 */
const PRIVATE_KEY = require('./gearth-210619-d70adabc5e28.json');
const PORT = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;



/**
 * autenticar en GEE e iniciar el servidor
 */
ee.data.authenticateViaPrivateKey(PRIVATE_KEY, () => {
    ee.initialize(null, null, () => {
        app.listen(PORT);
        console.log(`Listening on port ${PORT}`);
    });
});